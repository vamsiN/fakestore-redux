import { Component } from 'react'
import axios from 'axios'
import ProductCard from '../ProductCard/ProductCard'
import MenuSection from '../MenuSection/MenuSection'
import Footer from '../Footer/Footer'
import BannerSection from '../BannerSection/BannerSection'
import './ProductsPage.css'
import Error from '../Error/Error'
import Loader from '../Loader/Loader'
import { Link } from 'react-router-dom'

import { setProducts } from '../../redux/actions/productActions'
import { connect } from 'react-redux'
class ProductsPage extends Component {

    constructor(props) {
        super(props)

        this.state = {
            productsArray: [],
            status: this.APP_STATE.LOADING,


        }


    }

    APP_STATE = {
        LOADING: 'loading',
        LOADED: 'loaded',
        ERROR: 'error',
    }


    // getProducts = () => {

    //     console.log(this.props)

    //     axios.get('https://fakestoreapi.com/products')
    //         .then((response) => {
    //             this.setState({
    //                 productsArray: [...response.data,],
    //                 status: this.APP_STATE.LOADED
    //             }, () => {
    //                 console.log(this.state)
    //                 this.props.setProducts(response.data)
    //             })
    //         })
    //         .catch((error) => {
    //             console.error(error)
    //             this.setState({ status: this.APP_STATE.ERROR })
    //         })
    // }




    componentDidMount = () => {
        // console.log('mount')
        // this.getProducts()
        console.log(this.props)

        axios.get('https://fakestoreapi.com/products')
            .then((response) => {
                this.setState({
                    productsArray: [...response.data,],
                    status: this.APP_STATE.LOADED
                }, () => {
                    console.log(this.props.products)
                    if(!(this.props.products.length >=20)){
                        this.props.setProducts(response.data)
                    }
                    
                })
            })
            .catch((error) => {
                console.error(error)
                this.setState({ status: this.APP_STATE.ERROR })
            })





    }


    render() {

        const { status } = this.state

        console.log(this.props.products)

        return (
          

            <>


                    {status === this.APP_STATE.LOADING ?
                        <>  
                            <div className='mt-5'>
                                <Loader />
                            </div>
                            
                        </>
                        :
                        undefined
                    }
                    {status === this.APP_STATE.ERROR ?
                        <>
                            < Error />
                        </>
                        :
                        undefined
                    }
                    {status === this.APP_STATE.LOADED ?
                        <>
                            {this.props.products.lenth === 0 ?
                                <>
                                    No Products Available
                                </>
                                :

                                <>

                                    <div>
                                        <div className='container-fluid mx-auto '>
                                            <div className='row'>
                                                <div className='col-10  mx-auto'>
                                                    <MenuSection />
                                                    {/* <BannerSection /> */}
                                                    <h1 className='text-center text-danger m-5'>Our Products</h1>
                                                </div>
                                            </div>
                                            <div className='col-10 text-center mx-auto'>
                                                <Link to='/addProduct'>
                                                    <button className='btn btn-primary m-2 '>Add Product</button>
                                                </Link>
                                            </div>

                                            <div className='col-10 justify-content-spacearound mx-auto '>

                                                <ul className='products-container  text-decoration-none'>
                                                    {this.props.products.map((eachProduct) => (
                                                        <ProductCard eachProductDetails={eachProduct} key={eachProduct.id} />
                                                    ))}

                                                </ul>

                                            </div>
                                        </div>

                                    </div>
                                </>



                            }
                            <Footer />
                        </>
                        :
                        undefined
                    }



            </>        

            
        )

    }
}



const mapStatePropsTopProps = (state) => {
    // console.log(state)
    return {
        products: state.allProducts.products
    }
}

const mapDispatchToProps = {
    setProducts
}




export default connect(mapStatePropsTopProps, mapDispatchToProps)(ProductsPage)

//---------- this is without bootstrap, redux working -----------


// import { Component } from 'react'
// import axios from 'axios'
// import ProductCard from '../ProductCard/ProductCard'
// import './ProductsPage.css'
// import Error from '../Error/Error'
// import Loader from '../Loader/Loader'
// import { Link } from 'react-router-dom'

// import { setProducts } from '../../redux/actions/productActions'
// import { connect } from 'react-redux'

// class ProductsPage extends Component {

//     constructor(props) {
//         super(props)

//         this.state = {
//             productsArray: [],
//             status: this.APP_STATE.LOADING,

//             // userEnterData:props.userEnterData
//             // searchInput: ''
//         }
    

//     }

//     APP_STATE = {
//         LOADING: 'loading',
//         LOADED: 'loaded',
//         ERROR: 'error',
//     }

    
//     getProductsFromAPI = () => {
//         const {getApiDataArray,userEnterData}=this.props
       
//         axios.get('https://fakestoreapi.com/products')
//             .then((response) => {
//                 this.props.setProducts(response.data)
//                 this.setState({
//                     productsArray: response.data,
//                     status: this.APP_STATE.LOADED
//                 }, () => {
//                     console.log(this.state)
//                     // if(this.state.productsArray.length>0){
//                     //     getApiDataArray(this.state.productsArray)
//                     // }
                    

//                 })
//             })
//             .catch((error) => {
//                 console.error(error)
//                 this.setState({ status: this.APP_STATE.ERROR })
//             })
//     }

//     // onChangeSearchInput = event => { this.setState({ searchInput: event.target.value }) }


//     componentDidMount = () => {
//         console.log('mount')
//         this.getProductsFromAPI()
        



        
//     }


//     render() {

//         const { status } = this.state
//         // const {combinedProductsArray}=this.props
//         console.log(this.props.products)
//         // const searchResults = productsArray.filter(eachIssue => eachIssue.title.toLowerCase().includes(searchInput.toLowerCase()))


//         return (
//             <div className='bg-container'>
//                 <h1 className='dashboard-heading'>Our Products</h1>
//                 <div className='add-product-button-container'>
//                     <Link to='addProduct'>
//                         <button className='add-product-button'>Add product</button>
//                     </Link>

//                 </div>


//                 <div className='products-page'>
//                     {/* <div className="search-input-container">
//                         < BsSearch alt="search icon" className="search-icon" />
//                         <input type='search' className='search-input' placeholder='Search Products' onChange={this.onChangeSearchInput} value={searchInput} />
//                     </div> */}

//                     {status === this.APP_STATE.LOADING ?
//                         <>
//                             <Loader />
//                         </>
//                         :
//                         undefined
//                     }
//                     {status === this.APP_STATE.ERROR ?
//                         <>
//                             < Error />
//                         </>
//                         :
//                         undefined
//                     }
//                     {status === this.APP_STATE.LOADED ?
//                         <>
//                             {this.props.products.lenth === 0 ?
//                                 <>
//                                     No Products Available
//                                 </>
//                                 :

//                                 <ul className='products-container'>
//                                     {this.props.products.map((eachProduct) => (
//                                         <ProductCard eachProductDetails={eachProduct} key={eachProduct.id} />
//                                     ))}
//                                     {/* {userEnterData.map((eachProduct) => (
//                                         <ProductCard productDetails={eachProduct}  />
//                                     ))} */}
//                                 </ul>

//                             }
//                         </>
//                         :
//                         undefined
//                     }



//                 </div>


//             </div>
//         )

//     }
// }

// const mapStatePropsTopProps = (state) => {
//     // console.log(state)
//     return {
//         products: state.allProducts.products
//     }
// }

// const mapDispatchToProps = {
//     setProducts
// }




// export default connect(mapStatePropsTopProps, mapDispatchToProps)(ProductsPage)




// ---------- used ProductsPage as a functional component with hooks to access store and dispatch

// import { useEffect, useState } from 'react'
// import axios from 'axios'
// import ProductCard from '../ProductCard/ProductCard'
// import './ProductsPage.css'
// import Error from '../Error/Error'
// import Loader from '../Loader/Loader'
// import { Link } from 'react-router-dom'
// import { useSelector, useDispatch } from 'react-redux'

// import { setProducts } from '../../redux/actions/productActions'




// const ProductsPage = () => {
    // const products = useSelector((state) => state.allProducts.products)
    // const dispatch = useDispatch()

//     const [status, seStatus] = useState('loading')

//     const getProductsFromApi = () => {
//         axios.get('https://fakestoreapi.com/products')
//             .then((response) => {
//                 // console.log('inside api call')
//                 // console.log(response.data)
//                 seStatus('loaded')
//                 dispatch(setProducts(response.data))

//             })
//             .catch((error) => {
//                 seStatus('error')
//                 console.log(error)

//             })
//     }

//     useEffect(() => {
//         console.log('useEffect called')
//         getProductsFromApi();
//     }, [])

//     // console.log( products)


//     return (

//         <>

//             {status === 'loading' ?
//                 <Loader />
//                 :
//                 undefined
//             }

//             {status === 'error' ?
//                 < Error />
//                 :
//                 undefined
//             }

//             {status === 'loaded' ?

                // <>
                    // <div>
                    //     <div className='container-fluid bg-dark'>
                    //         <div className='row'>
                    //             <div className='col-12 '>
                    //                 <h1 className='text-center text-danger m-5'>Our Products</h1>
                    //             </div>
                    //         </div>
                    //         <Link to='/addProduct'>
                    //             <button className='btn btn-primary m-2 '>Add Product</button>
                    //         </Link>
                    //         <div className='row justify-content-spacearound'>
                    //             {products.map((eachProduct) => (
                    //                 < ProductCard eachProductDetails={eachProduct} key={eachProduct.id} />
                    //             ))}

                    //         </div>
                    //     </div>

                    // </div>
                // </>


//                 :
//                 undefined
//             }

//         </>
//     )
// }

// export default ProductsPage