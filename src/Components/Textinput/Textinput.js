const Textinput = (props) => {
    return (
        <>
            <label>{props.lable}</label>
            <input
                name={props.name}
                className='input-feild'
                type='text'
                placeholder={props.placeholder}
                onchange={props.onChange}
            />
            <span className='error-message'>
                {this.props.error.length > 0 ?
                    <div>{props.error}</div>
                    :
                    <div>&nbsp;</div>
                }
            </span>
        </>
    )
}

export default Textinput;