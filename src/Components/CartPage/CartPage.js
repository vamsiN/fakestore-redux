import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'

import { TiDelete } from 'react-icons/ti'
import { changeQuantity } from '../../redux/actions/productActions'

const CartPage = () => {

    const cart = useSelector((state) => state.allProducts.cart)
    const dispatch = useDispatch()

    const total = cart.reduce((total, each) => {
        return total += each.quantity * each.price
    }, 0)

    const increaseQuantity = (event) => {
        console.log(event.target.id)
        const modifiedCart = cart.map((eachCartItem) => {
            if (parseInt(eachCartItem.id) === parseInt(event.target.id)) {
                return { ...eachCartItem, quantity: parseInt(eachCartItem.quantity) + 1 }
            } else {
                return eachCartItem
            }
        })

        console.log(modifiedCart)

        dispatch(changeQuantity(modifiedCart))


    }
    const decreaseQuantity = (event) => {
        console.log(event.target.id)

        const modifiedCart = cart.map((eachCartItem) => {
            if (parseInt(eachCartItem.id) === parseInt(event.target.id) && parseInt(eachCartItem.quantity) > 1) {
                return { ...eachCartItem, quantity: parseInt(eachCartItem.quantity) - 1 }
            } else {
                return eachCartItem
            }
        })

        console.log(modifiedCart)

        dispatch(changeQuantity(modifiedCart))
    }

    const deleteCartItem = (event) => {
        console.log(event.target.id)

        console.log('inside delete Cart item event handler')
        console.log(cart)
        const slicedCart = cart.filter((eachCartItem) => {
            // console.log(parseInt(eachCartItem.id), parseInt(event.target.id))
            return parseInt(eachCartItem.id) !== parseInt(event.target.id)
        })

        console.log(slicedCart)

        dispatch(changeQuantity(slicedCart))

    }

    return (
        <div>

            {total === 0 ?
                <>
                    <div className='mt-5 col-12  mx-auto text-center'>
                        <div>
                            <h1 className='text-danger '>No Products in the cart</h1>
                        </div>
                        <div>
                            <Link to='/'>
                                <button className='btn btn-outline-danger btn-md  m-3' style={{ fontSize: '1vw' }}>View all products</button>
                            </Link>
                        </div>

                    </div>

                </>
                :
                <>

                    <h1 className='text-center text-success pt-3 pb-5'>Products in your cart</h1>
                    <div className='container-fluid' style={{ fontSize: "1vw" }}>

                        <div className='row'>
                            

                            <div className=' col-12 d-flex flex-row m-2  text-center'>
                                <div className='col-1 '>
                                    <p>Product Image</p>
                                </div>

                                <div className='col-11  d-flex flex-row '>
                                    <div className='col-3'>
                                        <p className='font-weight-bold'><strong>Title</strong></p>

                                    </div>
                                    <div className='col-2'>
                                        <p className='font-weight-bold'><strong>quantity</strong></p>

                                    </div>
                                    <div className='col-2'>
                                        <p className='font-weight-bold'><strong>Price</strong></p>

                                    </div>
                                    <div className='col-2'>
                                        <p className='font-weight-bold'><strong>value</strong></p>

                                    </div>

                                </div>
                            </div>
                            <hr style={{ width: "100%", marginLeft: "0" }} />
                            <>
                                {
                                    cart.map((eachCartItem) => {

                                        return (
                                            <div className=' col-12 d-flex flex-row m-2  ' style={{ fontSize: '1vw' }}>
                                                <div className='col-1'>
                                                    <img className=' img-thumbnail border-0' src={eachCartItem.image} alt="product-image" />
                                                </div>

                                                <div className='col-11  d-flex flex-row text-center my-auto'>
                                                    <div className='col-3'>

                                                        <p>{eachCartItem.title}</p>
                                                    </div>
                                                    <div className='col-2 '>

                                                        <button id={eachCartItem.id} className='btn btn-outline-secondary  btn-sm text-danger' onClick={decreaseQuantity}> - </button>
                                                        <span className='mx-1'> {eachCartItem.quantity} </span>
                                                        <button id={eachCartItem.id} className='btn btn-outline-secondary btn-sm text-success' onClick={increaseQuantity}>+</button>
                                                    </div>
                                                    <div className='col-2'>

                                                        <p>&#8377; {eachCartItem.price}</p>
                                                    </div>
                                                    <div className='col-2'>

                                                        <p>&#8377; {eachCartItem.price * eachCartItem.quantity}</p>
                                                    </div>
                                                    <div className='col-2' onClick={deleteCartItem}>
                                                        <button className='btn btn-danger btn-md text-white' style={{ fontSize: '1vw' }} id={eachCartItem.id}> <TiDelete />Remove</button>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </>
                            <hr style={{ width: "100%", marginLeft: "0" }} />

                            <div className=' col-12 d-flex flex-row m-2  text-center' style={{ fontSize: '2vw' }}>
                                <div className='col-1'>
                                    {/* <p>Product Image</p> */}
                                </div>

                                <div className='col-11  d-flex flex-row '>
                                    <div className='col-3'>
                                        {/* <p className='font-weight-bold'>Title</p> */}

                                    </div>
                                    <div className='col-2'>
                                        {/* <p className='font-weight-bold'>quantity</p> */}

                                    </div>
                                    <div className='col-2'>
                                        <p className='font-weight-bold'>Total: </p>

                                    </div>
                                    <div className='col-2'>
                                        <p className='font-weight-bold'>&#8377; {total.toFixed(2)}</p>

                                    </div>
                                </div>
                            </div>

                            <div className='col-10 text-left d-flex justify-content-between'>
                                <div>
                                    <Link to='/'>
                                        <button className='btn btn-outline-danger btn-md  m-3' style={{ fontSize: '1vw' }}>View all products</button>
                                    </Link>
                                </div>
                                <div>
                                    <Link to='/checkout'>
                                        <button className='btn btn-outline-success btn-md  m-3' style={{ fontSize: '1vw' }}>Proceed to checkout</button>
                                    </Link>
                                </div>

                            </div>


                        </div>

                    </div>

                </>
            }



        </div>
    )
}
export default CartPage