import { Link, useParams } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import Loader from '../Loader/Loader'
import NotFound from '../NotFound/NotFound'

import { connect } from 'react-redux'
import { addToCart } from '../../redux/actions/productActions'

import './ProductDetailsPage.css'
import axios from 'axios'




const ProductDetailsPage = (props) => {

    const PAGE_STATE = {
        LOADING: 'loading',
        LOADED: 'loaded',
        ERROR: 'error',
    }

    // const [isLoading, setIsLoading] = useState(PAGE_STATE.LOADING)

    const [cartText, setCartText] = useState('Add to Cart')

    const params = useParams()
    const { id } = params
    console.log(id)
    console.log(props.products)

    const selectedProductArray = props.products.filter((eachProduct) => {
        return `${eachProduct.id}` === id;
    })


    const selectedProduct = selectedProductArray[0]

    const addProductToCart = (event) => {

        setCartText('Added to Cart')
        console.log('inside addProductToCart handler handler')

        props.addToCart(selectedProduct)

    }





    return (


        <div className='container-fluid mt-5 p-3'>

            <div className='row d-flex justify-content-center mt-5'>
                <div className='col-10 col-md-5 text-center  '>
                    <img className='product-detail-image' src={selectedProduct.image} alt='product-img' />
                </div>
                <div className='col-10 col-md-5 '>
                    <p className='product-detail-category'>category: {selectedProduct.category}</p>
                    <h1 className='product-detail-title'>{selectedProduct.title}</h1>
                    <p className='product-detail-description'>{selectedProduct.description}</p>
                    <p className='product-detail-price'> Price: <span className='price'>${selectedProduct.price}</span></p>
                    <p className='product-detail-rating'>Rating: {selectedProduct.rating.rate} {`(${selectedProduct.rating.count})`}</p>
                    <div className='buttons-container'>
                        <button className='button cart-button' onClick={addProductToCart}>{cartText}</button>
                        {/* <button className='button buy-button'>Buy now</button> */}
                        <Link to='/'>
                            <button className='button products-button'>View all products</button>
                        </Link>
                    </div><div>

                    </div>


                    <Link to={`/editProduct/${selectedProduct.id}`}>
                        <div className='text-left m-2'>
                            <button className="btn  btn-primary  ">Edit Product</button>
                        </div>
                    </Link>
                </div>

            </div>


        </div>

        //----------------------------------------------------------


        // <>
        //     {isLoading === PAGE_STATE.LOADING ?
        //         <>
        //             <Loader />
        //         </>
        //         :
        //         undefined
        //     }

        //     {isLoading === PAGE_STATE.ERROR ?
        //         <>
        //             < NotFound />
        //         </>
        //         :
        //         undefined
        //     }

        //     {isLoading === PAGE_STATE.LOADED ?
        //         <>
        //             <div className='product-detail-container'>
        //                 <div className='product-detail-image-container'>
        //                     <img className='product-detail-image' src={productDetails.image} alt='product-img' />
        //                 </div>
        //                 <div className='product-detail-text-container'>
        //                     <p className='product-detail-category'>category: {productDetails.category}</p>
        //                     <h1 className='product-detail-title'>{productDetails.title}</h1>
        //                     <p className='product-detail-description'>{productDetails.description}</p>
        //                     <p className='product-detail-price'> Price: <span className='price'>${productDetails.price}</span></p>
        //                     <p className='product-detail-rating'>Rating: {productDetails.rating.rate} {`(${productDetails.rating.count})`}</p>
        //                     <div className='buttons-container'>
        //                         <button className='button cart-button'>Add to cart</button>
        //                         <button className='button buy-button'>Buy now</button>
        //                         <Link to='/'>
        //                             <button className='button products-button'>View all products</button>
        //                         </Link>


        //                     </div>
        //                 </div>

        //             </div>

        //             <div className='container-fluid '>

        //                 <div className='row d-flex justify-content-center'>
        //                     <div className='col-10 col-md-5 text-center  '>
        //                         <img className='product-detail-image' src={selectedProduct.image} alt='product-img' />
        //                     </div>
        //                     <div className='col-10 col-md-5 '>
        //                         <p className='product-detail-category'>category: {selectedProduct.category}</p>
        //                         <h1 className='product-detail-title'>{selectedProduct.title}</h1>
        //                         <p className='product-detail-description'>{selectedProduct.description}</p>
        //                         <p className='product-detail-price'> Price: <span className='price'>${selectedProduct.price}</span></p>
        //                         <p className='product-detail-rating'>Rating: {selectedProduct.rating.rate} {`(${selectedProduct.rating.count})`}</p>
        //                         <div className='buttons-container'>
        //                             <button className='button cart-button'>Add to cart</button>
        //                             <button className='button buy-button'>Buy now</button>
        //                             <Link to='/'>
        //                                 <button className='button products-button'>View all products</button>
        //                             </Link>
        //                         </div>

        //                         <Link to={`/editProduct/${selectedProduct.id}`}>
        //                             <div className='text-left m-2'>
        //                                 <button className="btn  btn-primary  ">Edit Product</button>
        //                             </div>
        //                         </Link>
        //                     </div>

        //                 </div>


        //             </div>

        //         </>
        //         :
        //         undefined
        //     }
        // </>


    )

}



const mapStatePropsTopProps = (state) => {

    return {
        products: state.allProducts.products
    }
}

const mapDispatchToProps = {
    addToCart
}

export default connect(mapStatePropsTopProps, mapDispatchToProps)(ProductDetailsPage)







