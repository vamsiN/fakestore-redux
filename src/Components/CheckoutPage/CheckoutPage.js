import { Component } from 'react'

import validator from 'validator'
import { Link } from 'react-router-dom'

import { connect } from 'react-redux'
import { addAddress } from '../../redux/actions/usersActions'
class CheckoutPage extends Component {

    state = {

        orderConfirmationMsg: '',

        line1: '',
        line2: '',
        city: '',
        state: '',
        pincode: '',
        mail: '',
        phone: '',

        successMsg: '',




        line1Error: '',
        line2Error: '',
        cityError: '',
        stateError: '',
        pincodeError: '',
        mailError: '',
        phoneError: '',

        addressSubmitted: false,


    }


    placeOrder = () => {

        if (this.state.addressSubmitted) {
            this.setState({
                orderConfirmationMsg: 'Your order has been placed succesfully'
            })
        } else {
            this.setState({
                orderConfirmationMsg: 'please fill the address to place your order'
            })
        }



    }

    handleInputchange = (event) => {
        this.setState({
            [event.target.id]: event.target.value
        })
    }

    onFormSubmission = (event) => {
        console.log('inside formsubmission')

        event.preventDefault()

        const {



            line1,
            line2,
            city,
            state,
            pincode,
            mail,
            phone,



            line1Error,
            line2Error,
            cityError,
            stateError,
            pincodeError,
            mailError,
            phoneError,


        } = this.state

        let sucess = true;

        if (!(validator.isAlpha(line1))) {
            this.setState({
                line1Error: 'please enter valid AddressLine1 '
            })
            sucess = false;
        } else {
            this.setState({
                line1Error: ''
            })
        }

        if (!(validator.isAlpha(line2))) {
            this.setState({
                line2Error: 'please enter valid AddressLine2'
            })
            sucess = false;
        } else {
            this.setState({
                line2Error: ''
            })
        }

        if (city === "") {
            this.setState({
                cityError: 'please enter valid city'
            })
            sucess = false;
        } else {
            this.setState({
                cityError: ''
            })
        }

        if (state === "") {
            this.setState({
                stateError: 'please enter valid state'
            })
            sucess = false;
        } else {
            this.setState({
                stateError: ''
            })
        }

        if (!(validator.isNumeric(pincode))) {
            this.setState({
                pincodeError: 'Please enter a valid pin '
            })
            sucess = false;
        } else {
            this.setState({
                pincodeError: ''
            })
        }

        if (!(validator.isEmail(mail))) {
            this.setState({
                mailError: 'Please enter a valid email '
            })
            sucess = false;
        } else {
            this.setState({
                mailError: ''
            })
        }

        if (!(validator.isMobilePhone(phone))) {
            this.setState({
                phoneError: 'Please enter a valid phoneNumber '
            })
            sucess = false;
        } else {
            this.setState({
                phoneError: ''
            })
        }

        if (sucess === true) {

            console.log("sucess")



            this.setState({
                successMsg: 'Your address has been added.',

                line1: '',
                line2: '',
                city: '',
                state: '',
                pincode: '',
                mail: '',
                phone: '',






                line1Error: '',
                line2Error: '',
                cityError: '',
                stateError: '',
                pincodeError: '',
                mailError: '',
                phoneError: '',

                addressSubmitted: 'true'


            }, () => {
                console.log(this.state)
            })

            console.log('success')


            const newAddress = {
                line1,
                line2,
                city,
                state,
                pincode,
                mail,
                phone
            }

            console.log('successfully address added ')
            console.log(newAddress)

            this.props.addAddress(newAddress)

        } else {
            this.setState({
                successMsg: ''
            })
        }



    }


    render() {


        return (

            <div className='container-fluid mt-5'>
                <h1 className='text-center mt-5 pt-5'>Checkout page</h1>


                <div class="accordion row " id="accordionExample text-left">

                    <div class="accordion-item col-10 mx-auto">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                DELIVERY ADDRESS
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body">



                                {/* <div className='col-12'>
                                    <strong>Select form your previous Address</strong>
                                </div> */}




                                <form onSubmit={this.onFormSubmission}>
                                    <strong>Enter NEW Address</strong>

                                    <div class="mb-3">
                                        <label htmlFor="line1" class="form-label">Address Line 1</label>
                                        <input type="text" class="form-control" id="line1" onChange={this.handleInputchange} value={this.state.line1} />
                                        <span className='error-message'>{this.state.line1Error.length > 0 ? <div>{this.state.line1Error}</div> : <div>&nbsp;</div>}</span>
                                    </div>
                                    <div class="mb-3">
                                        <label htmlFor="line2" class="form-label">Address Line 2</label>
                                        <input type="text" class="form-control" id="line2" onChange={this.handleInputchange} value={this.state.line2} />
                                        <span className='error-message'>{this.state.line2Error.length > 0 ? <div>{this.state.line2Error}</div> : <div>&nbsp;</div>}</span>
                                    </div>
                                    <div class="mb-3">
                                        <label htmlFor="city" class="form-label">City/Town</label>
                                        <input type="text" class="form-control" id="city" onChange={this.handleInputchange} value={this.state.city} />
                                        <span className='error-message'>{this.state.cityError.length > 0 ? <div>{this.state.cityError}</div> : <div>&nbsp;</div>}</span>
                                    </div>
                                    <div class="mb-3">
                                        <label htmlFor="state" class="form-label">State/UT</label>
                                        <input type="text" class="form-control" id="state" onChange={this.handleInputchange} value={this.state.state} />
                                        <span className='error-message'>{this.state.stateError.length > 0 ? <div>{this.state.stateError}</div> : <div>&nbsp;</div>}</span>
                                    </div>
                                    <div class="mb-3">
                                        <label htmlFor="pincode" className="form-label-sm">Postal / Zip Code</label>
                                        <input type="text" class="form-control" id="pincode" onChange={this.handleInputchange} value={this.state.pincode} />
                                        <span className='error-message'>{this.state.pincodeError.length > 0 ? <div>{this.state.pincodeError}</div> : <div>&nbsp;</div>}</span>
                                    </div>
                                    <div class="mb-3">
                                        <label htmlFor="mail" className="form-label-sm">E-mail</label>
                                        <input type="text" class="form-control" id="mail" onChange={this.handleInputchange} value={this.state.mail} />
                                        <span className='error-message'>{this.state.mailError.length > 0 ? <div>{this.state.mailError}</div> : <div>&nbsp;</div>}</span>
                                    </div>
                                    <div class="mb-3">
                                        <label htmlFor="phone" className="form-label-sm">Phonee</label>
                                        <input type="text" class="form-control" id="phone" onChange={this.handleInputchange} value={this.state.phone} />
                                        <span className='error-message'>{this.state.phoneError.length > 0 ? <div>{this.state.phoneError}</div> : <div>&nbsp;</div>}</span>
                                    </div>


                                    <button type="submit" class="btn btn-primary">Submit</button>

                                    <div className='mx-auto'>
                                        <span className='text-success'>{this.state.successMsg.length > 0 ? <div>{this.state.successMsg}</div> : <div>&nbsp;</div>}</span>
                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item col-10 mx-auto">
                        <h2 class="accordion-header" id="headingFour">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" checked>
                                PAYMENT OPTIONS
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <form>

                                    <div class="form-check  m-3">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" />
                                        <label class="form-check-label" for="exampleRadios1">
                                            UPI
                                        </label>
                                    </div>
                                    <div class="form-check  m-3">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2" />
                                        <label class="form-check-label" for="exampleRadios2">
                                            Wallets
                                        </label>
                                    </div>
                                    <div class="form-check  m-3">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" />
                                        <label class="form-check-label" for="exampleRadios3">
                                            Credit / Debit / ATM Card
                                        </label>
                                    </div>
                                    <div class="form-check  m-3">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios4" value="option4" />
                                        <label class="form-check-label" for="exampleRadios4">
                                            Net Banking
                                        </label>
                                    </div>
                                    <div class="form-check  m-3">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios5" value="option5" />
                                        <label class="form-check-label" for="exampleRadios5">
                                            Cash on Delivery
                                        </label>
                                    </div>
                                    <div class="form-check  m-3">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios6" value="option6" />
                                        <label class="form-check-label" for="exampleRadios6">
                                            EMI (Easy Installments)
                                        </label>
                                    </div>
                                </form>


                            </div>
                        </div>
                    </div>

                    <div className='text-center m-5'>
                        <button type="button" className="btn btn-primary btn-lg" onClick={this.placeOrder}>Place your order</button>
                    </div>


                    <div>
                        <span className='error-message'>{this.state.cityError.length > 0 ? <div>{this.state.cityError}</div> : <div>&nbsp;</div>}</span>
                    </div>
                </div>






            </div>
        )
    }
}



const mapStatePropsTopProps = (state) => {
    // console.log(state)
    return {
        products: state.allProducts.products
    }
}

const mapDispatchToProps = {
    addAddress
}




export default connect(mapStatePropsTopProps, mapDispatchToProps)(CheckoutPage)

