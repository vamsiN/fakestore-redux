import { Link } from 'react-router-dom'
import './ProductCard.css'
import { connect } from 'react-redux'
import {addToCart} from '../../redux/actions/productActions'
import { useState } from 'react'

const ProductCard = (props) => {
    const { eachProductDetails } = props

    const [cartText, setCartText] = useState('Add to Cart')

    const addProductToCart = (event) =>{

        setCartText('Added to Cart')


        console.log('inside addProductToCart handler handler')
    
        props.addToCart(eachProductDetails)
       
    }

   

    return (
        <>


            <div className='col-10 col-sm-6 col-md-5 col-lg-3 mb-1  text-decoraton-none card-container'>
                
                <div className="card h-80 text-center p-4  m-2  text-decoration-none"  >
                    <Link to={`/products/${eachProductDetails.id}`}>
                        <img src={eachProductDetails.image} className="card-img-top card-img-responsive card-fluid" height='320px' alt="product-image" />
                    </Link>
                    <div className="card-body text-decoration-none">
                        <h6 className="card-title ">{eachProductDetails.title.substring(0, 18)}...</h6>
                        <p className="card-text text-danger text-decoration-none">Price: $ {eachProductDetails.price}</p>
                        <p className='text-success text-decoration-none'>Rating: {eachProductDetails.rating.rate} {`(${eachProductDetails.rating.count})`}</p>
                        {/* <button className="btn btn-outline-primary m-1 text-decoration-none">Buy Now</button> */}
                        <button className="btn btn-outline-primary m-1 text-decoration-none" onClick={addProductToCart}>{cartText}</button>
                        
                    </div>
                </div>

                

         
                

                
            </div>



        </>

    )

}


const mapStatePropsTopProps = (state) => {

    return {
        // selectedProduct: state.allProducts.selectedProduct
    }
}

const mapDispatchToProps = {
    addToCart
}




export default connect(mapStatePropsTopProps, mapDispatchToProps)(ProductCard)


























// import { Link } from 'react-router-dom'
// import './ProductCard.css'

// const ProductCard = (props) => {

//     const { productDetails } = props
//     const { category, description, id, url, price, rating, title } = productDetails
   

//     return (

//         <Link to={`/products/${id}`} className='container-link'>
//             <div className='card-container'>
//                 <div className='card-image-container'>
//                     <img className='card-image' src={url} alt='productimg' />
//                 </div>
//                 <div className='card-details-container'>
//                     <p className='card-category'>category: {category}</p>
//                     <p className='card-title'>{title}</p>
//                     {/* <p className='card-description'>{description}</p> */}
//                     <p className='card-price'>Price: $ {price}</p>
//                     <p className='card-rating'>Rating: {rating.rate} {`(${rating.count})`}</p>
//                 </div>
//             </div>
//         </Link>

//     )

// }

// export default ProductCard