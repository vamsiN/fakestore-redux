import { BiError } from 'react-icons/bi'
import './Error.css'



export default function Error(props) {

    return (
        <>
            <div className='error-container'>
                < BiError /> An Error occured
            </div>
        </>
    )

}