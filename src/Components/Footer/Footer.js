import { RiLineHeight } from "react-icons/ri"

import { IoMdBriefcase } from "react-icons/io"
import { MdStars } from "react-icons/md"
import { HiGift } from "react-icons/hi"
import { MdHelp } from "react-icons/md"
import { MdCopyright } from "react-icons/md"







const Footer = () => {
    return (

        <div className='row  justify-content-center lh-1' style={{ backgroundColor: '#172337', fontSize: '1vw' }} >
            <div className='col-12 m-0' >
                <div className="row  p-4 justify-content-center' style={{ backgroundColor: '#172337' }}">

                    <div className="col-2 p-3" >
                        <h5 className='text-secondary' style={{ fontSize: '1vw' }}>About</h5>
                        <ul className="list-unstyled text-white " >
                            <li className="mb-2"><p>Contact Us</p></li>
                            <li className="mb-2"><p>Careers</p></li>
                            <li className="mb-2"><p>FlipKart Stories</p></li>
                            <li className="mb-2"><p>About Us</p></li>
                            <li className="mb-2"><p>Press</p></li>
                            <li className="mb-2"><p>Flipkart Wholesale</p></li>
                            <li className="mb-2"><p>Corporate Information</p></li>
                        </ul>
                    </div>
                    <div className="col-2 p-3" >
                        <h5 className='text-secondary' style={{ fontSize: '1vw' }} >Help</h5>
                        <ul className="list-unstyled text-white" >
                            <li className="mb-2"><p>Payments</p></li>
                            <li className="mb-2"><p>Shipping</p></li>
                            <li className="mb-2"><p>Cancellations</p></li>
                            <li className="mb-2"><p>Faq</p></li>
                            <li className="mb-2"><p>Report</p></li>
                        </ul>
                    </div>
                    <div className="col-2 p-3">
                        <h5 className='text-secondary' style={{ fontSize: '1vw' }}>Policy</h5>
                        <ul className="list-unstyled text-white" >
                            <li className="mb-2"><p>Return Policy</p></li>
                            <li className="mb-2"><p>Terms Of Use</p></li>
                            <li className="mb-2"><p>Security</p></li>
                            <li className="mb-2"><p>Privacy</p></li>
                            <li className="mb-2"><p>Sitemap</p></li>
                            <li className="mb-2"><p>EPR Compliance</p></li>
                        </ul>
                    </div>
                    <div className="col-2 p-3" >
                        <h5 className='text-secondary' style={{ fontSize: '1vw' }} >Social</h5>
                        <ul className="list-unstyled text-white" >
                            <li className="mb-2"><p>Facebook</p></li>
                            <li className="mb-2"><p>Twitter</p></li>
                            <li className="mb-2"><p>YoutTube</p></li>

                        </ul>
                    </div>

                    <div className="col-2 p-3" >
                        <h5 className='text-secondary' style={{ fontSize: '1vw' }}>Mail Us:</h5>
                        <ul className="list-unstyled text-white" >
                            <li className="mb-2"><p>FlipKart Internet Private Limited, Buildings Alyssa, Begonia & Clove Embassy Tech Village,

                                Outer Ring Road, Devarabeesanahalli Village,

                                Bengaluru, 560103,

                                Karnataka, India</p>
                            </li>
                        </ul>
                    </div>
                    <div className="col-2 p-3 " >
                        <h5 className='text-secondary' style={{ fontSize: '1vw' }} >Registered Office Address:</h5>
                        <ul className="list-unstyled text-white" style={{ fontSize: '1vw' }}>
                            <li className="mb-2"><p>Flipkart Internet Private Limited,

                                Buildings Alyssa, Begonia &

                                Clove Embassy Tech Village,

                                Outer Ring Road, Devarabeesanahalli Village,

                                Bengaluru, 560103,

                                Karnataka, India

                                CIN : U51109KA2012PTC066107

                                Telephone: 044-45614700</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <hr className='border border-secondary' />

                <div className='row d-flex justify-content-center '>
                    <div className='col-6 pb-4  d-flex justify-content-center '>
                        <div className='col-3  text-white border-danger'>
                            <span className='text-warning '><IoMdBriefcase /></span><span > Become a Seller</span>
                        </div>

                        <div className='col-3 text-white'>
                            <span className='text-warning'><MdStars /></span><span> Advertise</span>
                        </div>

                        <div className='col-3  text-white border-danger'>
                            <span className='text-warning '><HiGift /></span><span> Gift Cards</span>
                        </div>

                        <div className='col-2 text-white'>
                            <span className='text-warning'><MdHelp /></span><span> Help Center</span>
                        </div>

                    </div>


                    <div className='col-3 text-white'>
                        <span className='text-white pd-1 m-1'><MdCopyright /></span><span>2007-2022 FlipKart.com</span>
                    </div>

                    <div className='col-3 text-white'>
                        <img className='w-75' src='https://static-assets-web.flixcart.com/fk-p-linchpin-web/fk-cp-zion/img/payment-method_69e7ec.svg' />
                    </div>

                </div>


            </div>


        </div>
    )

}

export default Footer