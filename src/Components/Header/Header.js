

import { BsSearch } from 'react-icons/bs'
import { FaShoppingCart } from 'react-icons/fa'

import { MdNotifications } from 'react-icons/md'
import { RiQuestionnaireFill } from 'react-icons/ri'
import { IoIosTrendingUp } from 'react-icons/io'
import { HiDownload } from 'react-icons/hi'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'


const Header = (props) => {

    return (
        <>
            <div className='row  bg-primary  '>
                <div className='col-10 ps-0 m-auto text-center d-flex align-items-center '>



                    <div className='col-2 d-flex flex-column  justify-content-center '>
                        {/* <h1 className=' p-0 m-0 border border-success' style={{ fontSize: "6vw" }}>Flipkart</h1> */}
                        <Link to='/'>
                            <div className='col-10   text-center'>
                                <img height='30' width='120' className='img-fluid'
                                    src="//img1a.flixcart.com/www/linchpin/fk-cp-zion/img/flipkart-plus_8d85f4.png"
                                    alt="flipkart-logo"
                                />
                                {/* <h1 className='text-white p-0 m-0 ' style={{ fontSize: "4vw" }}>Flipkarts</h1> */}
                            </div>
                        </Link>
                        <div className='col-10  text-center  '>
                            <p className=' text-white p-0 m-0' style={{ fontSize: "1vw", fontStyle: 'italic' }}>Explore Pluss</p>
                            {/* <span>
                                <img className='m-1' width='15vw' src='//img1a.flixcart.com/www/linchpin/fk-cp-zion/img/plus_aef861.png' alt='explore plus' />
                            </span> */}
                        </div>
                    </div>



                    <div className='col-4  '>

                        {/* <div className="input-group ">
                            <input type="search" className="form-control flex-shrink-1" style={{ borderRightWidth: '0px', borderRadius: '0px', width: '16vw', height: '2vw', fontSize:'1vw' }} placeholder="Search for products" />
                            <span class="input-group-text p-1 " style={{ backgroundColor: 'white', borderLeftWidth: '0px', borderRadius: '0px', width:'2vw', height:'2vw', color:'black', fontSize:'10vw' }} id="basic-addon2"><BsSearch /></span> 
                             <span className='glyphicon glyphicon-search border border-danger text-dark ' style={{width:'2vw', color: 'black'}}></span>
                        </div> */}

                        {/* <form className="d-flex" role="search">
                            <input className="form-control my-auto " type="search" placeholder="Search for products" aria-label="Search" style={{ borderRadius: '0px', fontSize: "1vw", width: '25vw', height: '2vw' }} />
                            <button className="btn btn-success  text-primary" type="submit" style={{ backgroundColor: 'white', borderRadius: '0px', borderLeftWidth: '0px', height: '2vw' }}><span className='flex-shrink-1 m-1'><BsSearch /></span></button>
                        </form> */}

                        <div className="d-flex flex-row justify-content-left my-auto ">

                            <input type='search' className='form-control my-auto ' placeholder='' style={{ borderRadius: '0px', fontSize: "1vw", width: '25vw', height: '2vw', borderWidth: '0px', margin: '0px' }} />
                            <button className='btn btn-md  ps-0' style={{ backgroundColor: 'white', borderRadius: '0px', fontSize: "1vw", width: '2vw', height: '2vw', borderWidth: '0px', margin: '0px' }}>< BsSearch alt="search icon" className="text-primary  " style={{ fontSize: '1.5vw' }} /></button>

                        </div>



                    </div>


                    <div className='text-right col-1   d-flex flex-column align-items-center ' style={{}}>
                        {/* <h1 className=' p-0 m-0 border border-success' style={{ fontSize: "4vw" }}>Flipkart</h1> */}
                        <div className="dropdown-center ">
                            <button className="btn btn-btn-light btn-sm text-primary bg-white my-auto" style={{ borderRadius: '0px', fontSize: "1vw", width: '5vw', height: '2vw' }} type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Login
                            </button>
                            <ul class="dropdown-menu">
                                <li><a className="dropdown-item" href="#">New Customer <span className='text-primary'>Sign up</span></a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">My Profile</a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">Orders</a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">Whislist</a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">Rewards</a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">Gift Cards</a></li>
                            </ul>
                        </div>
                    </div>


                    <div className='col-2 flex-shrink-1  d-flex flex-column justify-content-center' style={{ fontSize: "1vw" }}>
                        <p className='text-light text-center my-auto  '>Become a Seller</p>
                    </div>


                    <div className='col-1 flex-shrink-1   d-flex flex-column justify-content-center '>

                        <div className="dropdown align-self-center  ">
                            <button className="btn btn-primary dropdown-toggle " type="button" data-bs-toggle="dropdown" aria-expanded="false" style={{ fontSize: "1vw" }}>
                                More
                            </button>
                            <ul className="dropdown-menu" style={{ fontSize: "2vw" }}>
                                <li><a className="dropdown-item" href="#">< MdNotifications />  Notifications</a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">< RiQuestionnaireFill />  24x7 Customer Care</a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">< IoIosTrendingUp />  Advertise</a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">< HiDownload />  Download App</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className='col-1 flex-shrink-1   d-flex flex-column justify-content-center' style={{ fontSize: "1vw" }}>
                        <Link to='/cart'>
                            <div>
                                <span className='   flex-grow-1 text-white m-1 '><FaShoppingCart /></span><span className='text-light   flex-grow-1 m2'> Cart <span className='badge text-warning bg-dark'>{props.cart.length}</span></span>
                            </div>
                        </Link>

                    </div>



                </div>

            </div>

        </>








        // <div className='row bg-primary pt-1 d-flex  justify-content-center' >
        //     <div className='col-2 flex-shrink-1 d-flex flex-column align-items-center justify-content-center   '>
        // <div className='col-10  text-center'>
        //     <img height='25'
        //         src="//img1a.flixcart.com/www/linchpin/fk-cp-zion/img/flipkart-plus_8d85f4.png"
        //         alt="flipkart-logo"
        //     />
        // </div>
        // <div className='col-10 text-center  '>
        //     <span className='' style={{ color: 'white'}}>Explore Plus</span>
        //     <span>
        //         <img width='15' src='//img1a.flixcart.com/www/linchpin/fk-cp-zion/img/plus_aef861.png' alt='explore plus' />
        //     </span>
        // </div>
        //     </div>


        //     <div className='col-3 flex-shrink-1 d-flex flex-column justify-content-center  '>
        // <div className="input-group">
        //     <input type="text" className="form-control flex-shrink-1" style={{ borderRightWidth: '0px', borderRadius:'0px' }} placeholder="Search for products brands and more" />
        //     <span class="input-group-text flex-shrink-1" style={{ backgroundColor: 'white', borderLeftWidth: '0px', borderRadius:'0px' }} id="basic-addon2"><BsSearch /></span>
        // </div>
        //     </div>



        //     <div className='col-1 flex-shrink-1   d-flex flex-column justify-content-center'>
        //         {/* <button className='btn btn-light text-primary w-100 mt-3 ps-3 pe-3'>Login</button> */}
        // <div className="dropdown-center   ">
        //     <button className="btn btn-btn-light text-primary bg-white " style={{  borderRadius:'0px' }} type="button" data-bs-toggle="dropdown" aria-expanded="false">
        //         Login
        //     </button>
        //     <ul class="dropdown-menu">
        //         <li><a className="dropdown-item" href="#">New Customer <span className='text-primary'>Sign up</span></a></li>
        //         <hr />
        //         <li><a className="dropdown-item" href="#">My Profile</a></li>
        //         <hr />
        //         <li><a className="dropdown-item" href="#">Orders</a></li>
        //         <hr />
        //         <li><a className="dropdown-item" href="#">Whislist</a></li>
        //         <hr />
        //         <li><a className="dropdown-item" href="#">Rewards</a></li>
        //         <hr />
        //         <li><a className="dropdown-item" href="#">Gift Cards</a></li>
        //     </ul>
        // </div>
        //     </div>


        // <div className='col-2 flex-shrink-1  d-flex flex-column justify-content-center'>
        //     <p className='text-light text-center m-2    '>Become a Seller</p>
        // </div>


        // <div className='col-1 flex-shrink-1 d-flex flex-column justify-content-center  '>

        //     <div className="dropdown align-self-center  ">
        //         <button className="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
        //             More
        //         </button>
        //         <ul className="dropdown-menu">
        //             <li><a className="dropdown-item" href="#">< MdNotifications />  Notifications</a></li>
        //             <hr />
        //             <li><a className="dropdown-item" href="#">< RiQuestionnaireFill />  24x7 Customer Care</a></li>
        //             <hr />
        //             <li><a className="dropdown-item" href="#">< IoIosTrendingUp />  Advertise</a></li>
        //             <hr />
        //             <li><a className="dropdown-item" href="#">< HiDownload />  Download App</a></li>
        //         </ul>
        //     </div>
        // </div>
        // <div className='col-1 flex-shrink-1   d-flex flex-column justify-content-center'>
        //     <div>
        //         <span className='   flex-grow-1 text-white m-1 '><FaShoppingCart /></span><span className='text-light   flex-grow-1 m2'>Cart</span>
        //     </div>
        // </div>
        // </div>

    )
}



const mapStatePropsTopProps = (state) => {

    return {
        cart: state.allProducts.cart
    }
}

const mapDispatchToProps = {

}




export default connect(mapStatePropsTopProps, mapDispatchToProps)(Header)