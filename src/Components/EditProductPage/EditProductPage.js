import { Link, useParams } from 'react-router-dom'
import { connect } from 'react-redux'
import { useState } from 'react'
import { editProduct } from '../../redux/actions/productActions'
import validator from 'validator'

const EditProductPage = (props) => {

    const params = useParams()
    const { id } = params
    console.log(id)
    console.log(props)

    const selectedProductArray = props.products.filter((eachProduct) => {
        return `${eachProduct.id}` === id;
    })


    let selectedProduct = selectedProductArray[0]
    console.log(selectedProduct)

    const [title, setTitle] = useState(`${selectedProduct.title}`)
    const [category, setCategory] = useState(`${selectedProduct.category}`)
    const [image, setImage] = useState(`${selectedProduct.image}`)
    const [description, setDescription] = useState(`${selectedProduct.description}`)
    const [price, setPrice] = useState(`${selectedProduct.price}`)
    const [ratingRate, setRatingRate] = useState(`${selectedProduct.rating.rate}`)
    const [ratingCount, setRatingCount] = useState(`${selectedProduct.rating.count}`)


    const [titleErr, setTitleErr] = useState("")
    const [categoryErr, setCategoryErr] = useState("")
    const [imageErr, setImageErr] = useState("")
    const [descriptionErr, setDescriptionErr] = useState("")
    const [priceErr, setPriceErr] = useState("")
    const [ratingRateErr, setRatingRateErr] = useState("")
    const [ratingCountErr, setRatingCountErr] = useState("")
    const [successMsg, setSuccessMsg] = useState('')


    const formValidation = () => {
        const titleErr = ''
        const categoryErr = ''
        const imageErr = ''
        const descriptionErr = ''
        const priceErr = ''
        const ratingRateErr = ''
        const ratingCountErr = ''
        const successMsg = ''

        let isValid = true;

        if (title === "") {
            setTitleErr('please enter valid title')
            isValid = false;
        } else {
            setTitleErr('')
        }

        if (category === "") {
            setCategoryErr('please enter valid category')
            isValid = false;
        } else {
            setCategoryErr('')
        }

        if (!(validator.isURL(image))) {
            setImageErr('please enter valid image')
            isValid = false;
        } else {
            setImageErr('')
        }

        if (description === "") {
            setDescriptionErr('please enter valid description')
            isValid = false;
        } else {
            setDescriptionErr('')
        }

        if (!(validator.isNumeric(price))) {
            setPriceErr('please enter valid price')
            isValid = false;
        } else {
            setPriceErr('')
        }

        if (parseInt(ratingRate) < 0 || parseInt(ratingRate) > 5 || ratingRate === '') {
            setRatingRateErr('please enter rating in the range 0 - 5')
            isValid = false;
        } else {
            setRatingRateErr('')
        }

        if (parseInt(ratingCount) < 0 || ratingCount === '') {
            setRatingCountErr('please enter valid rating count')
            isValid = false;
        } else {
            setRatingCountErr('')
        }


        if (isValid === true) {
            setSuccessMsg('changes have added to the product')
            const editedProductDetails = {
                id: id,
                title: title,
                category: category,
                description: description,
                price: price,
                image: image,
                rating: {
                    rate: ratingRate,
                    count: ratingCount,
                }
            }

            console.log(editedProductDetails)
            selectedProduct = editedProductDetails
            console.log(selectedProduct)

            props.editProduct(editedProductDetails)
        } else {
            setSuccessMsg('')
        }


    }

    const submitEditProdictForm = (event) => {
        event.preventDefault()

        formValidation()

        // const editedProductDetails = {
        //     id: id,
        //     title: event.target.title.value,
        //     category: event.target.category.value,
        //     description: event.target.description.value,
        //     price: event.target.price.value,
        //     image: event.target.image.value,
        //     rating: {
        //         rate: event.target.ratingRate.value,
        //         count: event.target.ratingCount.value,
        //     }
        // }


        // console.log(editProduct)
        // props.editProduct(editedProductDetails)

    }

    return (
        <div className='container-fluid  mt-5'>
            {/* <div className='row text-center'>
                <h1 className=''>Edit Product Page</h1>
            </div> */}

            <div className='row d-flex flex-column justify-content-center'>

                <div className='col-12'>
                    <h1 className='text-center text-danger mt-5'>Edit Product Details</h1>
                    <form onSubmit={submitEditProdictForm}>
                        <div class="form-group row">
                            <label for="id" class="col-sm-2 col-form-label">Id:</label>
                            <div class="col-sm-10">
                                <input type="text" id="id" name='id' readonly class="form-control-plaintext disable" value={selectedProduct.id} />

                            </div>
                        </div>
                        <div class="form-group row my-2">
                            <label for="title" class="col-sm-2 col-form-label">Title:</label>
                            <div class="col-sm-7">
                                <input type="text" id='title' name='title' class="form-control" value={title} onChange={(e) => { setTitle(e.target.value) }} />
                                <span className='error-message'>{titleErr.length > 0 ? <div>{titleErr}</div> : <div>&nbsp;</div>}</span>
                            </div>
                        </div>
                        <div class="form-group row my-2">
                            <label for="category" class="col-sm-2 col-form-label">Category:</label>
                            <div class="col-sm-7">
                                <input type="text" id='category' name='category' class="form-control" value={category} onChange={(e) => { setCategory(e.target.value) }} />
                                <span className='error-message'>{categoryErr.length > 0 ? <div>{categoryErr}</div> : <div>&nbsp;</div>}</span>
                            </div>
                        </div>
                        <div class="form-group row my-2">
                            <label for="description" class="col-sm-2 col-form-label">Description:</label>
                            <div class="col-sm-7">
                                <input type="text" id='description' name='description' class="form-control" value={description} onChange={(e) => { setDescription(e.target.value) }} />
                                <span className='error-message'>{descriptionErr.length > 0 ? <div>{descriptionErr}</div> : <div>&nbsp;</div>}</span>
                            </div>
                        </div>
                        <div class="form-group row my-2">
                            <label for="price" class="col-sm-2 col-form-label">Price:</label>
                            <div class="col-sm-7">
                                <input type="text" id='price' name='price' class="form-control" value={price} onChange={(e) => { setPrice(e.target.value) }} />
                                <span className='error-message'>{priceErr.length > 0 ? <div>{priceErr}</div> : <div>&nbsp;</div>}</span>
                            </div>
                        </div>
                        <div class="form-group row my-2">
                            <label for="image" class="col-sm-2 col-form-label">Image Url:</label>
                            <div class="col-sm-7">
                                <input type="text" id='image' name='image' class="form-control" value={image} onChange={(e) => { setImage(e.target.value) }} />
                                <span className='error-message'>{imageErr.length > 0 ? <div>{imageErr}</div> : <div>&nbsp;</div>}</span>
                            </div>
                        </div>
                        <div class="form-group row my-2">
                            <label for="ratingRate" class="col-sm-2 col-form-label">Rating Rate:</label>
                            <div class="col-sm-7">
                                <input type="text" id='ratingRate' name='ratingRate' class="form-control" value={ratingRate} onChange={(e) => { setRatingRate(e.target.value) }} />
                                <span className='error-message'>{ratingRateErr.length > 0 ? <div>{ratingRateErr}</div> : <div>&nbsp;</div>}</span>
                            </div>
                        </div>
                        <div class="form-group row my-2">
                            <label for="ratingCount" class="col-sm-2 col-form-label">Rating Count:</label>
                            <div class="col-sm-7">
                                <input type="text" id='ratingCount' name='ratingCount' class="form-control" value={ratingCount} onChange={(e) => { setRatingCount(e.target.value) }} />
                                <span className='error-message'>{ratingCountErr.length > 0 ? <div>{ratingCountErr}</div> : <div>&nbsp;</div>}</span>
                            </div>
                        </div>
                        <div className='text-center my-2'>
                            <button type='submit' className='btn btn-primary'>submit</button>
                            <span className='error-message'>{ratingCountErr.length > 0 ? <div>{ratingCountErr}</div> : <div>&nbsp;</div>}</span>
                        </div>
                        <p className='text-success' id='succesMessage' name='succesMessage'>{successMsg}</p>
                        <Link to='/'>
                            <div className='text-center m-3'>
                                <button className='btn btn-success'>View All Products</button>
                            </div>
                        </Link>


                    </form>
                </div>

                <div className='col-12 my-2 border border-danger'>
                    <div className='row text-center'>
                        <h1 className=''>Current Product Details</h1>
                    </div>

                    <div>
                        <p className='my-2 '>id: <span className='mx-3'>{selectedProduct.id}</span></p>
                        <p className='my-2'>Title: <span className='mx-3'>{selectedProduct.title}</span> </p>
                        <p className='my-2'>Category: <span className='mx-3'>{selectedProduct.category}</span></p>
                        <p className='my-2'>Description: <span className='mx-3'>{selectedProduct.description}</span></p>
                        <p className='my-2'>Price: <span className='mx-3'>{selectedProduct.price}</span></p>
                        <p className='my-2'>Image Url: <span className='mx-3'>{selectedProduct.image}</span></p>
                        <p className='my-2'>Rating Rate: <span className='mx-3'>{selectedProduct.rating.rate}</span></p>
                        <p className='my-2'>Rating Count: <span className='mx-3'>{selectedProduct.rating.count}</span></p>

                    </div>


                </div>

            </div>

        </div>
    )
}



const mapStatePropsTopProps = (state) => {

    return {
        products: state.allProducts.products
    }
}

const mapDispatchToProps = {
    editProduct
}


export default connect(mapStatePropsTopProps, mapDispatchToProps)(EditProductPage)

