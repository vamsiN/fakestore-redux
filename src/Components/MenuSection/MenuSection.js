const MenuSection = () => {
    const menuOptionsArray = [
        {
            image: 'https://rukminim1.flixcart.com/flap/128/128/image/f15c02bfeb02d15d.png?q=100',
            name: 'Top Offers'
        },
        {
            image: 'https://rukminim1.flixcart.com/flap/128/128/image/29327f40e9c4d26b.png?q=100',
            name: 'Grocery'
        },
        {
            image: 'https://rukminim1.flixcart.com/flap/128/128/image/22fddf3c7da4c4f4.png?q=100',
            name: 'Mobiles'
        },
        {
            image: 'https://rukminim1.flixcart.com/flap/128/128/image/c12afc017e6f24cb.png?q=100',
            name: 'fashion'
        },
        {
            image: 'https://rukminim1.flixcart.com/flap/128/128/image/69c6589653afdb9a.png?q=100',
            name: 'Electronics'
        },
        {
            image: 'https://rukminim1.flixcart.com/flap/128/128/image/ab7e2b022a4587dd.jpg?q=100',
            name: 'Home'
        },
        {
            image: 'https://rukminim1.flixcart.com/flap/128/128/image/0ff199d1bd27eb98.png?q=100',
            name: 'Appliances'
        },
        {
            image: 'https://rukminim1.flixcart.com/flap/128/128/image/71050627a56b4693.png?q=100',
            name: 'Travel'
        },
        {
            image: 'https://rukminim1.flixcart.com/flap/128/128/image/dff3f7adcf3a90c6.png?q=100',
            name: 'Beauty, Toys'
        }
    ]

    return (
        <div className='row mt-5' >
            <div className='col-12  d-flex flex-row justify-content-center'>
                {menuOptionsArray.map((eachMenuItem) => {
                    return (

                        <div className='col-1  text-center' >
                            <img className='text-center p-3' style={{ width: '8vw', height: '8vw' }} src={eachMenuItem.image} />
                            <p className='text-left ms-2' style={{ fontSize: '1vw' }}>{eachMenuItem.name}</p>
                        </div>

                    )
                })}
            </div>
        </div>
    )
}

export default MenuSection