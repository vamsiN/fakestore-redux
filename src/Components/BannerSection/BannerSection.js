const BannerSection = () => {
    return (
        <div className='row me-5 ms-5 mt-3 border border-danger'>
            <div className='col-12 mx-auto border border-danger '>
                <div id="carouselExampleIndicators" className="carousel slide border border-danger col-12" data-bs-ride="true">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="https://rukminim1.flixcart.com/fk-p-flap/844/140/image/89e8178047a5c857.jpg?q=50" className="d-block w-100 image-fluid" alt="A" />
                        </div>
                        <div class="carousel-item">
                            <img src="https://rukminim1.flixcart.com/fk-p-flap/844/140/image/09ddba4a718005ca.jpg?q=50" className="d-block w-100 image-fluid" alt="A" />
                        </div>

                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>

            </div>
        </div>

    )
}

export default BannerSection