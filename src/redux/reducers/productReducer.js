import { v4 as uuid } from 'uuid'

import ActionTypes from '../constants/action-types'

const initialState = {
    products: [],
    cart: []
}

const productReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case ActionTypes.SET_PRODUCTS:
            if (state.products.length === 0) {
                return { ...state, products: payload };
            } else {
                return state;
            }


        case ActionTypes.ADD_PRODUCT:
            return { ...state, products: [...state.products, { ...payload, id: uuid() }] };



        case ActionTypes.EDIT_PRODUCT:
            console.log('inside edit product reducer')
            const modifiedProducts = state.products.map((eachProduct) => {

                if (eachProduct.id.toString() === payload.id) {
                    console.log(typeof (eachProduct.id), typeof (payload.id), eachProduct.id.toString() === payload.id)

                    return payload
                } else {
                    return eachProduct
                }
            })
            return { ...state, products: modifiedProducts };

        case ActionTypes.ADD_TO_CART:
            const present = state.cart.filter((each) => parseInt(payload.id) === parseInt(each.id))

            if (present.length) {
                const newCart = state.cart.map((each) => {

                    if (parseInt(payload.id) === parseInt(each.id)) {
                        return { ...each, quantity: each.quantity + 1 }
                    } else {
                        return each
                    }


                })


                return { ...state, cart: [...newCart] };
            } else {

                return { ...state, cart: [...state.cart, { ...payload, quantity: 1, }] };
            }

        case ActionTypes.CHANGE_QUANTITY:
            return { ...state, cart: payload };

        default:
            return state;
    }
}

export default productReducer