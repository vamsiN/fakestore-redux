import { combineReducers } from 'redux'

import productReducer from './productReducer'
import usersReducer from './usersReducer'
// import cartReducer from './cartReducer'


const reducers = combineReducers({
    allProducts: productReducer,
    users: usersReducer,


})

export default reducers