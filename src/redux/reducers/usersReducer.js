import { v4 as uuid } from 'uuid'

import ActionTypes from '../constants/action-types'

const initialState = {
    usersList: [],
    address: []
}

const usersReducer = (state = initialState, { type, payload }) => {
    console.log('inside userReducer')
    switch (type) {
        case ActionTypes.ADD_ADDRESS:
            if (state.address.length === 0) {
                return { ...state, address: [...state.address, payload] };
            } else {
                return state;
            }




        default:
            return state;
    }
}

export default usersReducer