import ActionTypes from '../constants/action-types'

export const setProducts = (products) => {
    return {
        type: ActionTypes.SET_PRODUCTS,
        payload: products,
    };
}

export const addProduct = (product) => {
    return {
        type: ActionTypes.ADD_PRODUCT,
        payload: product,
    };
}

export const editProduct = (product) => {
    console.log('inside editproduct action creator')
    return {
        type: ActionTypes.EDIT_PRODUCT,
        payload: product,
    };
}

export const addToCart = (product) => {
    console.log(product)
    console.log('inside addToCart Action creator')
    return {
        type: ActionTypes.ADD_TO_CART,
        payload: product,
    };
}

export const changeQuantity = (modifiedCart) => {
    // console.log(product)
    // console.log('inside addToCart Action creator')
    return {
        type: ActionTypes.CHANGE_QUANTITY,
        payload: modifiedCart,
    };
}





