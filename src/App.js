// function App() {
//   return (
//     <div className="App">
//       <h1>hello</h1>
//     </div>
//   );
// }

// export default App;

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import ProductDetailsPage from "./Components/ProductDetailsPage/ProductDetailsPage";
import ProductsPage from './Components/ProductsPage/ProductsPage'
import NotFound from './Components/NotFound/NotFound'
import AddProductForm from "./Components/AddProductForm/AddProductForm";
import EditProductPage from "./Components/EditProductPage/EditProductPage";
import Header from './Components/Header/Header'
import CartPage from "./Components/CartPage/CartPage";
import CheckoutPage from "./Components/CheckoutPage/CheckoutPage";

import './App.css';
import { Component } from "react";


class App extends Component {


  render() {
    return (
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<ProductsPage />} />
          <Route path="/products/:id" element={<ProductDetailsPage />} />
          <Route path="/addProduct" element={< AddProductForm />} />
          <Route path="/editProduct/:id" element={< EditProductPage />} />
          <Route path="/cart" element={<CartPage />} />
          <Route path="/checkout" element={<CheckoutPage />} />

          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    )

  }




}

export default App;
